<?php 
	
class TenTenDataDotCom{
	
	var $post_url = 'https://www2.1010data.com/cgi-bin/prod-latest/gw';
	var $dir, $domain, $homepage, $kill, $pfrom, $uid, $pswd, $ptype;
	var $keys;
	var $sid;
	var $epswd;
	var $api;
	
	//initiator function
	function __construct($uid, $pswd){
			
		$session = array();
		$session['uid'] = $this->uid = $uid;
		$session['pswd'] = $this->pswd = $pswd;
		$this->set_session($session);
		
		$this->dir = 0;
		$this->domain = 'www2.1010data.com';
		$this->homepage = 'prod-latest';
		$this->kill = 'possess';
		$this->pfrom = 'home';
		
			
	}
	
	//setting session only for this class
	function set_session($data = array()){
		if(!empty($data)){
			foreach($data as $key => $value){
				$_SESSION['tenten'][$key] = $value;
			}
		}
	}
	
	//return teh session only for this class
	function get_session($key){
		return $_SESSION['tenten'][$key];
	}
	
	//unsetting session. it can be unset from anywhere
	static function unset_session(){
		$_SESSION['tenten'] = array();
	}
	
	
	/**
	 * get the membership
	 * */
	public function get_membership(){
		$this->refresh_authentication();
		$this->api = 'getuser';
		
		$this->keys = array('api', 'uid', 'pswd');
		
		$param = $this->get_body();
				
		if(!empty($this->sid) && !empty($this->epswd)){
			$param['sid'] = $this->sid;
			$param['pswd'] = $this->epswd;
		}
		
		$response = $this->curl_handle($param);
		return $response;		
	}
	
	
	/**
	 * parse the rc
	 * */
	function get_rc($response){
		$regex = '#<rc>(.*?)</rc>#';
		return $this->match($regex, $response, 1);
	}
	
	/**
	 * return the message
	 * */
	function get_msg($response){
		$regex = '#<msg>(.*?)</msg>#';
		$msg = $this->match($regex, $response, 1);
		return strlen($msg) > 2 ? $msg : $response;
	}
	
	
	/**
	 * get groups
	 * */
	function get_groups($response){
		
		//var_dump($response); return;
		
		$regex = '#groups="(.*?)"#';
		$group_string = $this->match($regex, $response, 1);
		$groups = array();
		
		if($group_string){
			$groups = explode(' ', trim($group_string));
		}
		
		return $groups;
	}
	
	
	//flushing the authentication. Reauthenticate if needed	
	function refresh_authentication(){
		if(!$this->get_session('is_loggedin')){
			$this->login();
		}
		
		$this->sid = $this->get_session('sid');
		$this->epswd = $this->get_session('epswd');
		$this->kill = '';
	}
	
	
	//function to hanlde the login and to geth encrypted password
	function login(){
		
		//if already logged in skip
		$this->keys = array('dir', 'domain', 'ptype', 'homepage', 'kill', 'pfrom', 'uid', 'pswd');
		
		$this->ptype = 'login';
		$this->sid = null;
		$this->epswd = null;
		
		//var_dump($this->get_body());
		
		$info = $this->curl_handle($this->get_body());
		
		$headers = get_headers($this->post_url, 1);
		var_dump($headers);
		
		$new_headers = apache_response_headers();
		var_dump($new_headers);
		
		$login_info = array();
		
		if($info){			
			$param = $this->parse_login_info($info);
			if(isset($param['sid']) && isset($param['pswd'])){
				$login_info['sid'] = $param['sid'];
				$login_info['epswd'] = $param['pswd'];
				$login_info['is_loggedin'] = true;
			}
			
			$this->set_session($login_info);
		}
				
		
	}
	
	
	function parse_login_info($info){
		$pattern = '#<a href="(.*?)">.*?</a>#';
		
		$return = array();
		
		if(preg_match('#<a href="(.*?)">.*?</a>#', $info, $matches)){
			$url = $matches[1];
			$parse_url = parse_url($url);			
			
			if(isset($parse_url['query'])){
				$query = explode('&amp;', $parse_url['query']);			
				
				foreach($query as $q){
					$qq = explode('=', $q);					
					$return[trim($qq[0])] = trim($qq[1]);					
				}
			}
		}
		
		return $return;
	}
	
	
	//get the body
	function get_body(){
		$param = array();
		foreach ($this->keys as $key){
			if(strlen($key) > 1){
				$param[$key] = $this->$key;
			}
		}	
		
		return $param;
	}
	
	
	//make post request
	function request($param){
		
		$args = array(
					'body' => $param,
					);
		
		return wp_remote_post($this->post_url, $args);
	}
	
	
	function curl_handle($fields){
		
		$ch = curl_init($this->post_url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, 50);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt($ch, CURLOPT_NOBODY, true);
		
		if($fields){
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($fields));
		}
		
		if($headers){
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		}
		
		$response = curl_exec($ch);
		$status = curl_getinfo($ch);
		
		curl_close($ch);

		//var_dump($response);
		
		return $response;		
	}

	
	/*
	 * regex match
	 * */
	function match($regex, $string, $position){
		if(preg_match($regex, $string, $matches)){
			return $matches[$position];
		}
		else{
			return false;
		}	
	}
	
	//this will set session remotely
	static function remotely_set_session($data = array()){
		if(!empty($data)){
			foreach($data as $key => $value){
				$_SESSION['tenten'][$key] = $value;
			}
		}
	}
	
}

?>